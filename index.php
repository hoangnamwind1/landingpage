<!DOCTYPE html>
<html>
<head>
	<title>Landing Page</title>

	<meta name="lang" content="en">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<script type="text/javascript" src="js/landingpage.js"></script>
	
	<!-- Loading bootstrap css and js  and JQUERY-->
  <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>



	<link rel="stylesheet" type="text/css" href="css/landingpage.css">
</head>
<body>

	<div class="container-fluid">

		<div class="row container-top">

			<div class="col-md-6 col-lg-6 header-left">
			<a href="#"><img src="img/logo.png" class="img-responsive"></a>
			<a href="#"><img src="img/download-btn.png" class="img-responsive"></a>
			</div>
			<a class="col-md-6 col-lg-6 header-right" href="#"><img src="img/phone-hand.png"></a>
			

		</div>

		<div class="row container-middle">
			<center class="row">
			<h1>8 TIỆN ÍCH MIỄN PHÍ KHI BẠN CÓ 123PHIM</h1>
			<P>Đã có hơn 500,000 người lựa chọn. Còn bạn?</P>
			</center>

				<div class="row container-middle-advantage">
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-1.jpg"></a>
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-2.jpg"></a>
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-3.jpg"></a>
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-4.jpg"></a>
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-5.jpg"></a>
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-6.jpg"></a>
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-7.jpg"></a>
					<a href="#" class="col-md-4 col-sm-6 col-lg-3 col-xs-12 advantage-item"><img src="img/uu-diem-8.jpg"></a>
					<div class="col-md-4 col-m4-offset-8 col-sm-6 col-sm-ofset-6 col-lg-4 col-lg-offset-8 col-xs-12 tai-app-ngay"><a href="#"><img src="img/tai-app-ngay-btn.png" class="img-responsive"></a></div>
					</div>
				</div>

			<div class="line">
			</div>
			

			<div class="row container-middle-1">

				<a class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-android" href="#"><img class="img-responsive" src="img/android.png"></a>

				<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 container-middle-1-text">
				<h2> Không mất thời gian tìm lịch chiếu ở nhiều nơi</h2>
				<hr>
				<p>Chỉ cần mở ứng dụng 123Phim, bạn có thể xem lịch chiếu của tất cả các bộ phim ở rạp chiếu bất kì</p>
				<br>
				<p>Ngoài ra, kho tin tức điện ảnh hằng ngày và review sẽ giúp bạn giải trí và chọn phim đúng &quot;<strong>GU</strong> &quot;nhất</p>
				</div>

				<a href="#"><img class="bg2-image" src="img/bg2.png"></a>
			</div>

			<div class="row container-bottom">
					<center class="row">
						<h1>Quên Chuyện Xếp Hàng với <button class="btn btn-danger">3</button> bước đơn giản</h1>
					</center>

					<div class="row">
						<div class="col-lg-6 col-lg-offset-3 content">
							<button class="btn btn-danger">1</button> Tải App 123PHIM</br></br>
							<button class="btn btn-danger">2</button> Xem lịch chiếu phim yêu thích</br></br>
							<button class="btn btn-danger">3</button> Mua vé và thanh toán nhanh chóng</br>

						</div>
					</div>

					<div class="row download-btn">
						<div class="col-lg-6 col-lg-offset-3"><a href="#"><img src="img/download-btn.png"></a></div>
					</div>
			</div>
	<div class="row footer">
		<div class="footer-left" > <img src="img/logovng.jpg"></div>
		<div class="footer-right" > <h3>123PHIM- SẢN PHẨM CỦA CT CP DV MẠNG VI NA- MỘT THÀNH VIÊN CỦA CÔNG TY CỔ PHẦN VNG</h3>
		<p> Địa chỉ: 322B Lý Thường Kiệt, Phường 14, Quận 10, TP HCM<br>
		Số DKKD 0305418909, cấp ngày 29/12/2007- Sở KHDT TP HCM cấp</p></div>
	</div> 

	</div>
</body>
</html>

<div></div>